

    Describe 'Testing Get-InstalledSecurityProducts' {


        Context 'chars in computer name' {

            $testCases = @(
                @{
                    ComputerName = 'sdfsdfsdf'
                    TestName = 'Empty computer name'
                }
                @{
                    ComputerName = '!sdc**c33'
                    TestName = 'Computer name with symbols'
                }
                @{
                    ComputerName = 'this one'
                    TestName = 'Computer name with spaces'
                }
                @{
                    ComputerName = ','
                    TestName = 'Computer name as comma'
                }
                @{
                    ComputerName = 'PC 8459'
                    TestName = 'Computer name as comma'
                }
            )


            It 'should be a string: <TestName>' -TestCases $testCases {
                param($ComputerName)

                $ComputerName | Should BeOfType System.String

            }

            #It 'should not contain a space: <TestName>' -TestCases $testCases {
            #    param($ComputerName)
#
 #               $ComputerName | Should Not Contain ' '

  #          }
        }


        Context 'is null or empty' {

            $testCases = @(
                @{
                    ComputerName = $null
                    TestName = 'Null computer name'
                }
            )


            It 'should be null or empty: <TestName>' -TestCases $testCases {
                param($ComputerName)

                $ComputerName | Should BeNullOrEmpty

            }

        }


        Context 'Quantity of computer names specified' {

            $testCases = @(

                for ($i = 1; $i -lt 33; $i++) {

                    @{
                        ComputerName = "a$i"
                        TestName = 'More than 32 computers specified'
                    }

                })

            It 'should be 32 or less computers' -TestCases $testCases {
                $ComputerCount = $testCases.Count

                $ComputerCount | Should BeLessThan 33

            }


        }

}
