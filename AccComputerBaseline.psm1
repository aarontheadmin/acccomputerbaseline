# Define tools path
[string]$ToolsPath = "$PSScriptRoot\tools"

# Define supporting files path
[string]$SupportFilesContainerName = 'support'

# Assemble support files absolute path
[string]$SupportFilesPath = Join-Path -Path $ToolsPath -ChildPath $SupportFilesContainerName

#[string]$BaselineToolbox = 'C:\BaselineToolbox.ps1'


# Load supporting functions into memory.
try {
	
    $params = @{
        Path = $SupportFilesPath
        Filter = '*.ps1'
        File   = $true
		ErrorAction = 'Stop'
    }

    $SupportFiles = Get-ChildItem @params #| Where-Object -FilterScript { $_.Extension -match "ps1$" }

    $SupportFiles | ForEach-Object { . $_.FullName }
	
}
catch {

    Write-Verbose "Could not load support files. Execution halted."

    Break

}

# Load tools into memory.
try {

	$params = @{
        Path  = $ToolsPath
        Filter = '*.ps1'
        File   = $true
		ErrorAction = 'Stop'
    }

    $ToolFiles = Get-ChildItem @params #| Where-Object -FilterScript { $_.Extension -match "ps1$" }

	$ToolFiles | ForEach-Object { . $_.FullName }
	
} catch {

	Write-Verbose "Could not load tools. Execution halted."

	Break
	
}


<#
Function Update-BaselineToolbox {
	#
	. SYNOPSIS
	Compiles a .ps1 script file for execution on remote computers.

	. DESCRIPTION
	This cmdlet copies content from tool and support script files,
	and saves everything in one .ps1 file. This file is shipped to
	remote computers for local execution as some tools access the
	registry for information. This allows collection of registry
	data without having to enable the Remote Registry service on
	(a growing number of) remote computers on the network.

	. EXAMPLE

	PS C:\>Update-BaselineToolbox

	To update tools in Baseline Toolbox, type:

		Update-BaselineToolbox -ConfirmUpdate

	This will create a new file and update all tools
	in the file so it can be shipped to remote systems
	for local execution.

	Executing this cmdlet without parameters returns the informative
	message above.

	. EXAMPLE

	PS C:\>Update-BaselineToolbox -ConfirmUpdate

		Directory: C:\


		Mode                LastWriteTime         Length Name
		----                -------------         ------ ----
		-a----       2016-12-13  10:03 AM             45 BaselineToolbox.ps1


		Tools have been updated in C:\BaselineToolbox.ps1!

	To compile the .ps1 file (overwrites existing by default), run
	the cmdlet above with the -ConfirmUpdate parameter. It is followed
	by an informative message that the file was created.

	#
	[CmdletBinding(SupportsShouldProcess = $true)]
	[OutputType([System.String])]
    Param ()

    if ($PSCmdlet.ShouldProcess()) {

		Write-Verbose "Confirm switch specified."
        # Attempt to create a new file/overwrite existing and
        # update the content in it from each tool source file.
        try {

			Write-Verbose "Creating new .ps1 file."
        
            New-Item -Path $BaselineToolbox `
                     -ItemType File `
                     -Force `
                     -Value "[string]`$ComputerName = `$env:ComputerName`r`n`r`n" `
					 -ErrorAction Stop


            foreach ($Support in $SupportFiles) {

				Write-Verbose "Updating .ps1 script file from support file: $($Support.Name)."

                Add-Content -Path $BaselineToolbox `
                            -Value (Get-Content -Path $Support.FullName)

            }


            foreach ($Tool in $ToolFiles) {

				Write-Verbose "Updating .ps1 script file from tool file: $($Tool.Name)."

                Add-Content -Path $BaselineToolbox `
                            -Value (Get-Content -Path $Tool.FullName) `
							-ErrorAction Stop

            }


            foreach ($Tool in $ToolFiles) {

				Write-Verbose "Updating .ps1 script file with function call $($Tool.BaseName)."

                Add-Content -Path $BaselineToolbox `
                            -Value ($Tool.BaseName) `
							-ErrorAction Stop

            }
            
            Write-Output "`r`n`r`nTools have been updated in $BaselineToolbox!"
    
        } catch {

			Write-Verbose "Failed to build .ps1 script file."
			
            Write-Warning "Could not update $BaselineToolbox"

        }
    
    } else {

        @"

        To update tools in Baseline Toolbox, type:

                Update-BaselineToolbox -ConfirmUpdate

        This will create a new file and update all tools
        in the file so it can be shipped to remote systems
        for local execution.
        
"@
    } # if

} # Update-BaselineToolbox
#>


<#
Function Send-BaselineToolbox {

    [CmdletBinding(SupportsShouldProcess = $true)]
    Param (
        [Parameter(Mandatory = $true, Position = 1)]
        [ValidateCount(1,32)]
        [string[]]$ComputerName
    )

	if ($PSCmdlet.ShouldProcess()) {

		$results = Invoke-Command -ComputerName $ComputerName -FilePath $BaselineToolbox

		Write-Output $results
	}

}
#>


<#
#
. SYNOPSIS
This cmdlet obtains baseline information for each computer specified in the -ComputerName parameter.

. DESCRIPTION
This cmdlet obtains baseline information from each computer specified in the -ComputerName 
parameter. Each specified computer is tested for network connectivity, and then responsive 
computers stored in a queue for issuing remote commands to it (the queue can be used later 
to determine what computers were baselined, and also to report if a baseline failed before 
it executed on that computer - due to shutdown or network issues). Unresponsive computers 
are not added to the queue.

Baseline folders are created on each computer (if they do not already exist), including the 
backup server where a folder for each computer is created. The folders are created with 
the Domain Admins security group having FullControl. Each computer's baseline is exported 
to XML and stored locally on the computer and copied to the backup server.

. NOTES
===========================================================================
 Author:		Aaron Hardy
 Organization:	AccurIT Technology Solutions
 Contact:		aaron@accurit.ca
===========================================================================

. PARAMETER ComputerName
Names of computers, separate by a comma.

. EXAMPLE
Get-BaselineReport -ComputerName Server-DC1

. EXAMPLE
Get-BaselineReport -ComputerName Server-DC1,FileServer5,DNS02

#>
<#
Function Start-Baseline
{
	[CmdletBinding()]
	param
	(
		[Parameter(Position = 1,
			 Mandatory = $True,`
			 HelpMessage = "Specify between 1 and 10 computer names, separated by a comma. CNAME and FQDN is not supported.")]
		[ValidateCount(1, 10)]
		[ValidateNotNullOrEmpty()]
		[string[]]$ComputerName
	)

	[string[]]$OnlineComputers = @();
	
	Write-Verbose "`r`nPerforming preliminary checks..."
	Write-Verbose "`r`n`tChecking if computers are online..."
	foreach ($Computer in $ComputerName)
	{
		if ((Test-Connection -ComputerName $Computer -Quiet -Count 1 -ErrorAction Stop))
		{
			try
			{
				$Permissions = "cbwoffice\Domain Admins", "FullControl", "ContainerInherit,ObjectInherit", "None", "Allow"
				
				Invoke-Command `
							   -ComputerName $Computer `
							   -ArgumentList $LocalExportRootPath `
							   -ErrorAction Stop `
							   -Command { `
					param ($LocalExportRootPath)
					
					if (-not (Test-Path $LocalExportRootPath))
					{
						Write-Verbose "`tCreating local baseline store on $using:Computer..."
						New-Item -Path $LocalExportRootPath -ItemType Directory -Force | Out-Null
						$Acl = Get-Acl $LocalExportRootPath
						$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule $using:Permissions
						
						$Acl.SetAccessRule($AccessRule)
						
						$Acl | Set-Acl -Path $LocalExportRootPath
					}
				} # End Invoke-Command
				
				Invoke-Command `
							   -ComputerName $BackupServer `
							   -ErrorAction Stop `
							   -Command { `
					[string]$BackupServerStoreRoot = 'B:\Backups\SystemConfigBaselines'
					[string]$ServerRootPath = "$BackupServerStoreRoot\$using:Computer"
					
					# Create the root folder on the backup server if it does not exist.
					if (-not (Test-Path ("$BackupServerStoreRoot")))
					{
						Write-Verbose "`tCreating baseline store on backup server $using:BackupServer..."
						New-Item -Path $BackupServerStoreRoot -ItemType Directory -Force | Out-Null
						$Acl = Get-Acl -Path $BackupServerStoreRoot
						$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule $using:Permissions
						$Acl.SetAccessRule($AccessRule)
						$Acl | Set-Acl -Path $BackupServerStoreRoot
					}
					
					# Create the computer folder in the root folder on the backup server 
					# if it does not exist.
					if (-not (Test-Path ("$ServerRootPath")))
					{
						Write-Verbose "`tCreating baseline store for $using:Computer on $using:BackupServer..."
						New-Item -Path $ServerRootPath -ItemType Directory -Force | Out-Null
						
						# Permissions are inherited from the parent folder.				
					}
				} # End Invoke-Command
				
                $OnlineComputers += $Computer
				
			}
			catch
			{
				Write-Warning "`tCould not connect to ""$Computer"" and/or create its local baseline store."
			}
		}
		else
		{
			Write-Warning "`tUnable to establish connection with system ""$Computer"" $(Get-Date)"
		}
		
	} # End foreach

    if ($OnlineComputers.Count -gt 0)
    {
        $RemoteSession = New-PSSession -ComputerName $OnlineComputers -Credential $Credential -ErrorAction Stop
		
	    Write-Verbose "`r`n`tOnline Computers:"
	    Write-Verbose "`r`n`t`t$OnlineComputers" | Sort-Object | Format-Wide -Column 4
	    Write-Verbose "`r`nExecuting baselines online computers..."
		
	    Invoke-Command `
					    -Session $RemoteSession -FilePath C:\Scripts\ComputerBaseline\CBFunctions.ps1 `
					    -ArgumentList $LocalExportRootPath, $RemoteExportRootPath, $BackupServer `
					    -ErrorAction Stop
		
	    Write-Verbose "`r`nComplete.`r`n`r`n"
    }
    else
    {
        Write-Warning "`tNo computers specified to start baseline.`r`n`r`n"
    }
	
} # End Function Start-Baseline


# Execute Invoke-BaselineFunctions and pipe to Export-CliXML 
# to export the output to the remote computer. Export the 
# file name as "SCB_[SERVERNAME]_[yyyyMMddHHmmss].xml".
#Invoke-BaselineFunctions | Export-Clixml $LocalExportPath

# Copy the XML file from the remote computer to the backup server.
#Copy-Item -Path $LocalExportPath -Destination $RemoteExportPath -Force
#} # End of $ScriptBlock


#>
#Export-ModuleMember -Function Get-InstalledSecurityProducts,Get-SystemInfo,Get-OSInfo,Get-RemoteDesktopState,Get-WUSetting,Get-PsConfiguration,Get-NetworkConfiguration,Get-DiskInfo