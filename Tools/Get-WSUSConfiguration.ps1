$wsusServer = Get-WsusServer -Name diadem -Port 8530

$configs = $wsusServer.getconfiguration()

$wsusServer | gm

$wsusSubscription = $wsusServer.GetSubscription()
$selectedProducts = $wsusSubscription.GetUpdateCategories() | Select Title


$selectedClassifications = $wsusSubscription.GetUpdateClassifications() | Select Title


$configs | ForEach-Object { Export-Csv -Path C:\users\ahardysa\Desktop\config.csv -NoTypeInformation -Append }


$csv = Import-csv -Path C:\users\ahardysa\Desktop\config.csv -Header Name,Value -Delimiter ','

foreach ($p in $csv) {

    $props = @{ Name = $p.Name;
                Value = $p.Value }

    $obj = New-Object -TypeName psobject -Property $props

    Write-Output $obj

}