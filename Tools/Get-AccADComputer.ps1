﻿Function Get-AccADComputer {

    <#
    .SYNOPSIS
    This cmdlet returns specially-filtered computer 
    objects from Active Directory.

    .DESCRIPTION
    This cmdlet returns computer objects from Active
    Directory, like Get-ADComputer, but it also
    provides custom switch parameters to filter the
    objects based on what is specified in the
    Description field found in the computer object's
    properties.

    For example, specifying the -PhysicalOnly switch
    will exclude all computer objects that are not
    'Physical' computers.

    .PARAMETER All
    Using the -All switch parameter will return all
    computer object types (Physical, VM, etc.), in
    the specified Path, as it does not provide any
    filtering.

    .PARAMETER PhysicalOnly
    Using the -PhysicalOnly switch parameter will
    return all computer objects, in the specified
    Path, where "Physical" is specified in the 
    properties Description in Active Directory.

    .PARAMETER VMOnly
    Using the -VMOnly switch parameter will return
    all computer objects, in the specified Path,
    where "Vhpv" (Hyper-V VM) is specified
    in the properties Description in Active
    Directory.

    .PARAMETER Path
    The Active Directory container to search in.

    .PARAMETER MaxDaysLastLogon
    The maximum number of days since a computer
    object last logged on to Active Directory.
    The default is 90 days if not manually
    specified.

    .PARAMETER Credential
    The credential to be used for authenticating
    the request with Active Directory. This
    requires Domain Administrator privileges.
    #>

    [CmdletBinding(DefaultParameterSetName = 'All')]
    Param (
        
        [Parameter(ParameterSetName = 'All')]
        [switch]$All,
        
        [Parameter(ParameterSetName = 'PhysicalOnly')]
        [switch]$PhysicalOnly,
        
        [Parameter(ParameterSetName = 'VMOnly')]
        [switch]$VmOnly,

        [Parameter(Position = 0)]
        [string]$Path,

        [ValidateRange(1, 1095)]
        [uint16]$MaxDaysLastLogon = 90,

        [Parameter(HelpMessage = 'Credentials to query AD objects.')]
        [pscredential]$Credential
    )
            
    try {

        [datetime]$CutoffDate = (Get-Date).AddDays( - ($MaxDaysLastLogon))
            
        Write-Verbose "Getting computer objects from Active Directory."

        $props = @{
            Credential  = $Credential
            Filter      = { LastLogonTimestamp -gt $CutoffDate }
            ErrorAction = 'Stop'
        }

        if (-not ([string]::IsNullOrEmpty($Path))) {
            $props.Add('SearchBase', $Path)
        }
            
        $ComputerName = Get-ADComputer @props -Property Description, LastLogonTimeStamp |
            Select-Object -Property Description,
            @{
                Name       = 'ComputerName';
                Expression = { $_.Name } },
            @{
                Name       = 'LastLogonTimeStamp';
                Expression = { [datetime]::FromFileTime($_.LastLogonTimeStamp) }},
            DistinguishedName,
            SID,
            Enabled |
            Where-Object {
            if ($PhysicalOnly) { $_.Description -eq 'Physical' }
            elseif ($VMOnly) { $_.Description -eq 'Vhpv' }
            else { $_.Name -ne '' } # if
        } # Where

        Write-Output $ComputerName

    }
    catch {

        Write-Warning "Failed to get computer(s) from Active Directory."

    } # try/catch

} # function