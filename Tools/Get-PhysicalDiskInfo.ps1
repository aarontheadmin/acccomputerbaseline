﻿Function Get-PhysicalDiskInfo {
    <#

    .SYNOPSIS
    Gets all physical disks from local or remote computers. This
    cmdlet supports Windows 7/Server 2008 SP2 and later.

    .DESCRIPTION
    This cmdlet can query local or remote computers for all
    physical disks, including SCSI configuration settings.

    .PARAMETER ComputerName
    The ComputerName parameter can accept from 1 to 32 computers, separated by a comma.

    .EXAMPLE
    Get-PhysicalDiskInfo -ComputerName CL1

    ComputerName     : CL1
    Device           : 0
    Capacity         : 931.51 GB
    Model            : ST1000DM003-1CH162
    InterfaceType    : IDE
    SCSIBus          : 0
    SCSILogicalUnit  : 0
    SCSIPort         : 0
    SCSITargetId     : 0
    FirmwareRevision : CC49
    SerialNumber     : 44TP90DFG

    .EXAMPLE
    Get-PhysicalDiskInfo CL1,CL2,DC01 | select ComputerName,Capacity,InterfaceType,SerialNumber

    ComputerName Capacity  InterfaceType SerialNumber
    ------------ --------  ------------- ------------
    CL1          931.51 GB IDE           44TP90DFG
    CL2          931.51 GB IDE           WD-WCC3F2LHAE1S
    DC01         80 GB     IDE

    #>

	[CmdletBinding()]
	Param (
		[Parameter(Mandatory = $true,
                   Position = 1)]
		[ValidateCount(1,32)]
		[string[]]$ComputerName
	)

    [string]$Namespace = 'ROOT\CIMv2'
    [string]$ClassName = 'Win32_DiskDrive'
	
    
    Write-Verbose "$vm_EnumerateComputers $($ComputerName -join ', ')."

	foreach ($Computer in $ComputerName) {

        [string]$Computer = $Computer.ToUpper()
        [string]$ObjAboutComputer = "PhysicalDiskObj for ""$Computer""."

        try {

            Write-Verbose "Loading class $ClassName on ""$Computer""."

            $splat = @{'Namespace' = $NameSpace;
                       'ClassName' = $ClassName;
                       'ComputerName' = $Computer;
                       'ErrorAction' = 'SilentlyContinue'}

            $AllDisks = Get-CimInstance @splat |
                        Select-Object DeviceID,
                                      FirmwareRevision,
                                      Size,
                                      SerialNumber,
                                      InterfaceType,
                                      Model,
                                      SCSIBus,
                                      SCSILogicalUnit,
                                      SCSIPort,
                                      SCSITargetId,
                                      Index

            Write-Verbose "Enumerating disks on ""$Computer""."

            foreach ($disk in $AllDisks) {

                Write-Verbose "$vm_CreateHashtable $ObjAboutComputer"

			    $props = [Ordered]@{
                    "ComputerName" = $Computer;				    
                    "Device" = $disk.Index;
				    "Capacity" = (IsEmpty ("$([math]::round(($disk.Size/1GB), 2)) GB"));
				    "Model" = $disk.Model;
				    "InterfaceType" = $disk.InterfaceType;
				    "SCSIBus" = $disk.SCSIBus;
				    "SCSILogicalUnit" = $disk.SCSILogicalUnit;
				    "SCSIPort" = $disk.SCSIPort;
				    "SCSITargetId" = $disk.SCSITargetId;
				    "FirmwareRevision" = $disk.FirmwareRevision;
				    "SerialNumber" = $disk.SerialNumber.toString().Trim() }

			    
                Write-Verbose "$vm_InitiateInstanceOfNewObject $ObjAboutComputer"
                
                $PhysicalDiskObj = New-Object -TypeName PSObject -Property $props -ErrorAction SilentlyContinue

                Write-Verbose "$vm_ReturnObjOutput $ObjAboutComputer"
                
                Write-Output $PhysicalDiskObj

            } # End foreach disk

        } catch {

            Write-Verbose "$vm_ErrorInitiateInstanceOfNewObject $ObjAboutComputer"
            
        } # End try/catch

    } # End foreach computer

} # End Get-PhysicalDiskInfo function