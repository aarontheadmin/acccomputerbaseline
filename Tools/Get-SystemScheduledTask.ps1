Function Get-SchTask {

    <#
    .SYNOPSIS
    Gets all scheduled task information from all users.

    .DESCRIPTION
    This function queries the system for scheduled tasks
    under all principals using the command line tool 
    schtasks.exe for Windows operating systems
    earlier than Windows 7 SP 1 and Server 2008 R2.

    #>

    [CmdletBinding()]
    [OutputType([System.Array])]
    param (
        [Parameter(Mandatory = $true,
                   Position = 1)]
        [string[]]$ComputerName
    )

    @(foreach ($Computer in $ComputerName) {

        #Write-Host -ForegroundColor Green "Processing $Computer ..."

        $ErrorActionPreference = 'SilentlyContinue'

        Write-Verbose "Initializing schtasks.exe on ""$Computer""."

        $TaskText = schtasks.exe /query /s $Computer /fo list /v

        if (-not $?) {
            Write-Warning -Message "$Computer`: schtasks.exe failed"
            continue
        }

        $ErrorActionPreference = 'Continue'
        $TaskText = $TaskText -join "`n"
        
        Write-Verbose "Looping through scheduled tasks on ""$Computer""."

        @(
            foreach ($m in @([regex]::Matches($TaskText, '(?ms)^Folder:[\t ]*([^\n]+)\n(.+)'))) {
            
                $Folder = $m.Groups[1].Value

                foreach ($FolderEntries in @($m.Groups[2].Value -split "\n\n")) {

                    foreach ($Inner in $FolderEntries) {

                        $regex = '(?m)^((?:Repeat:\s)?(?:Until:\s)?[^:]+):[\t ]+(.*)'

                        [regex]::Matches([string] $Inner, $regex) |
                            ForEach-Object -Begin {

                                    $h = @{}; $h.'Folder' = [string] $Folder

                                } -Process {

                                    $h.($_.Groups[1].Value) = $_.Groups[2].Value

                                } -End {

                                    New-Object -TypeName PSObject -Property $h

                                }
                    }
                }
            }) |
        Where-Object { $_.Folder -notlike '\Microsoft*' -and `
            $_.'Task To Run' -notmatch 'COM handler' } #| Format-List $_.'Run As User' -notmatch '^(?:SYSTEM|LOCAL SERVICE|Everyone|Users|Administrators|INTERACTIVE)$' -and `
    })

    Write-Verbose "Looping completed."

}



Function Get-SystemScheduledTask {

    <#

    .SYNOPSIS
    Gets all scheduled task information from all users
    using Get-Scheduled or schtasks.exe.

    .DESCRIPTION
    This function queries the system for scheduled tasks
    under all principals using the Get-ScheduledTask
    cmdlet for computers running Windows 7 SP 1/Server 
    2008 R2 and newer.

    Computers running older operating systems use the
    schtasks.exe command line tool.

    #>

    [CmdletBinding()]
    [OutputType([System.Array])]
    param (
        [Parameter(Mandatory = $true,
                   Position = 1)]
        [string[]]$ComputerName
    )

    Write-Verbose "Enumerating computers: $($ComputerName.ToUpper() -join ', ')"

    foreach ($Computer in $ComputerName) {

        $Computer = $Computer.ToUpper()

        <#
        try {

            $ScheduledTask = Get-ScheduledTask -ErrorAction Stop |
                             Select-Object -Property TaskName,
                                                     TaskPath,
                                                     State,
                                                     Actions,
                                                     Author,
                                                     Date,
                                                     Description,
                                                     Documentation,
                                                     Principal,
                                                     SecurityDescriptor,
                                                     Settings,
                                                     Source,
                                                     Triggers,
                                                     URI,
                                                     Version

            $Actions = $ScheduledTask.Actions
            $Principal = $ScheduledTask.Principal
            $Settings = $ScheduledTask.Settings
            $Triggers = $ScheduledTask.Triggers
            $TriggersRepetition = $Triggers.Repetition

            Write-Output $TriggersRepetition

        } catch [System.Management.Automation.CommandNotFoundException] {
        #>
            #Write-Warning "$($MyInvocation.MyCommand.Name) cmdlet not recognized by ""$Computer""."

            Write-Verbose "Getting scheduled tasks on ""$Computer""."

            Get-SchTask -ComputerName $Computer |
            Format-List -Property @{ n= 'HostName'; e= { $_.HostName.ToUpper()}},
                                  TaskName,
                                  'Next Run Time',
                                  Status,
                                  'Logon Mode',
                                  'Last Run Time',
                                  'Last Result',
                                  Author,
                                  'Task To Run',
                                  'Start In',
                                  Comment,
                                  'Scheduled Task State',
                                  'Idle Time',
                                  'Power Management',
                                  'Run As User',
                                  'Delete Task If Not Rescheduled',
                                  'Stop Task If Runs X Hours and X Mins',
                                  Schedule,
                                  'Schedule Type',
                                  'Start Time',
                                  'Start Date',
                                  'End Date',
                                  Days,
                                  Months,
                                  'Repeat: Every',
                                  'Repeat: Until: Time',
                                  'Repeat: Until: Duration',
                                  'Repeat: Stop If Still Running'
        
        Write-Verbose "Completed getting scheduled tasks on ""$Computer""."
        <#
        } catch {

            Write-Warning "Could not get scheduled task(s) from ""$Computer""."
            Break

        }
        #>    
    }

    Write-Verbose "Completed getting scheduled tasks on $($ComputerName.ToUpper() -join ', ')."

}