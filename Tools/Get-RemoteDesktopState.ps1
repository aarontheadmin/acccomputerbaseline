﻿Function Get-RemoteDesktopState {
    <#

    .SYNOPSIS
    Obtains the state of Remote Desktop.

    .DESCRIPTION
    This function obtains the status, port, and NLA status of 
    Remote Desktop for the local computer.

    NOTE: This function can only be used locally and not on remote systems.

    .EXAMPLE
    Get-RemoteDesktopState

    ComputerName RDPStatus RDPPort NLA
    ------------ --------- ------- ---
    PC1          Enabled      3389 Enabled

    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true,
                   Position = 1)]
        [string[]]$ComputerName
    )

    #[string]$Namespace = 'ROOT\CIMv2\TerminalServices'
    #[string]$IsUnreadable = 'Unreadable'


$ScriptBlock = {

        [string]$Namespace = 'ROOT\CIMv2\TerminalServices'
        [string]$IsUnreadable = 'Unreadable'

        $splat = @{'Namespace' = $Namespace;
                        'ClassName' = 'Win32_TerminalServiceSetting';
                        'ErrorAction' = 'Stop'}

        $RdpStatus = `
                Get-CimInstance @splat |
                Select-Object -ExpandProperty AllowTSConnections


        Write-Verbose "Opening RDP-Tcp registry key for PortNumber value."

        $RegSubKey = 'System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp'
        $Reg = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]::LocalMachine, $env:COMPUTERNAME)

        $RegKey= $Reg.OpenSubKey($RegSubKey)
        Write-Verbose "Registry key successfully opened."

        $PortNumber = $RegKey.GetValue("PortNumber")
        Write-Verbose "Value in registry key is $PortNumber."

        $splat = @{'Namespace' = $Namespace;
                        'ClassName' = 'Win32_TSGeneralSetting';
                        'ErrorAction' = 'SilentlyContinue'}

        $NLA = Get-CimInstance @splat |
                Select-Object -ExpandProperty UserAuthenticationRequired


        [string]$ObjAboutComputer = "RDPStateObj for $ComputerName."


        Write-Verbose "$vm_CreateHashtable $ObjAboutComputer"

        $props = [Ordered]@{
                RDPStatus    = $RdpStatus;
                PortNumber   = $PortNumber;
                NLA          = $NLA
        }

        Write-Verbose "$vm_InitiateInstanceOfNewObject $ObjAboutComputer"

        $RDPStateObj = New-Object -TypeName PSObject -Property $props -ErrorAction SilentlyContinue

        Write-Verbose "$vm_ReturnObjOutput $ObjAboutComputer"

        Write-Output $RDPStateObj
                
    } # End Script Block

    $splat = @{'ComputerName' = $ComputerName;
               'ErrorAction' = 'SilentlyContinue';
               'ScriptBlock' = $ScriptBlock}

    Invoke-Command @splat |
    Select-Object -Property `
                        @{ n = 'ComputerName'; e = {
                                Write-Output $_.PSComputerName.ToUpper() }},
                        @{ n = 'RDPStatus'; e = { Convert-IntToWord $_.RDPStatus } },
                        PortNumber,
                        @{ n = 'NLA'; e = { Convert-IntToWord $_.NLA } }

} # End Get-RemoteDesktopState function