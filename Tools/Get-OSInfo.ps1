﻿Function Get-OSInfo {
    <#

    .SYNOPSIS
    Obtains core information about the operating system of 
    local or remote computers.

    .DESCRIPTION
    This cmdlet can query local or remote computers for their 
    operating system, OS version and the computer's product type, 
    also referred to as "Role in Network". Roles include (client,
    member server, or domain controller).

    .PARAMETER ComputerName
    The ComputerName parameter can accept from 1 to 32 computers, separated by a comma.

    .EXAMPLE
    Get-OSInfo -ComputerName DC01

    ComputerName    : DC01
    OperatingSystem : Microsoft Windows Server 2012 R2 Standard
    SPVersion       : 0
    OSVersion       : 6.3.9600
    RoleInNetwork   : Domain Controller

    .EXAMPLE
    Get-OSInfo DC01,SRV02,FS03 | Format-Table

    ComputerName OperatingSystem                           SPVersion OSVersion RoleInNetwork
    ------------ ---------------                           --------- --------- -------------
    DC01        Microsoft Windows Server 2012 R2 Standard         0 6.3.9600  Domain Controller
    SRV02       Microsoft® Windows Server® 2008 Standard          2 6.0.6002  Member Server
    FS03        Microsoft Windows Server 2012 R2 Standard         0 6.3.9600  Member Server

    #>

	[CmdletBinding()]
	Param (
		[Parameter(Mandatory = $true,
                   HelpMessage = 'Enter one or more computer names.')]
		[ValidateCount(1,32)]
		[string[]]$ComputerName
	)


    Write-Verbose "$vm_EnumerateComputers $($ComputerName -join ', ')."

	foreach ($Computer in $ComputerName) {

        [string]$ObjAboutComputer = "for $Computer."

        try {

            $ClassName = 'CIM_OperatingSystem'
		    
            Write-Verbose "Loading class $ClassName on $Computer."

            $splat = @{'Namespace' = 'ROOT\CIMv2';
                       'ClassName' = $ClassName;
                       'ComputerName' = $Computer;
                       'ErrorAction' = 'Stop'}

                    

            $ComputerOS = Get-CimInstance @splat |
                          Select-Object Caption,
                                        ProductType,
                                        ServicePackMajorVersion,
                                        Version

            $ProductType     = $ComputerOS.ProductType
            $OperatingSystem = $ComputerOS.Caption
            $SPMajorVersion  = $ComputerOS.ServicePackMajorVersion
            $OSVersion       = $ComputerOS.Version

        } catch {

            Write-Error $Error[0] #"Failed to access class $ClassName on $Computer."

        }

        Write-Verbose "Checking if $Computer is Client, Member Server or Domain Controller."
        Write-Verbose "Initializing switch() to query value $ProductType $ObjAboutComputer"

        $RoleInNetwork = switch ($ProductType) {
                            1 { 'Client' }
                            2 { 'Domain Controller' }
                            3 { 'Member Server' }
                            default { 'Unknown' } }

        Write-Verbose "switch() value refers to ""$RoleInNetwork"" $ObjAboutComputer"
        

        try {

            Write-Verbose "$vm_CreateHashtable OSInfoObj $ObjAboutComputer"

            $props = [Ordered]@{"ComputerName" = $Computer.ToUpper();
			                    "OperatingSystem" = $OperatingSystem;
                                "SPVersion" = $SPMajorVersion;
                                "OSVersion" = $OSVersion;
                                "RoleInNetwork" = $RoleInNetwork }

            Write-Verbose "$vm_InitiateInstanceOfNewObject $ObjAboutComputer"

		    $OSInfoObj = New-Object -TypeName PSObject -Property $props -ErrorAction Stop

            Write-Verbose "$vm_ReturnObjOutput $ObjAboutComputer"

            Write-Output $OSInfoObj

        } catch {

            Write-Error $Error[0] #"$vm_ErrorInitiateInstanceOfNewObject OSInfoObj $ObjAboutComputer"

        }
	}

} # End Get-OSInfo function