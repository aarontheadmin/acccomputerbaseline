﻿# Regkey does not exist on Windows 10.10493
Function Get-WUSetting {
    <#

    .SYNOPSIS
    Obtains the Windows Update setting.

    .DESCRIPTION
    This function obtains the Windows Update setting of 
    the local computer.

    NOTE: This function can only be used locally and not on remote systems.

    .EXAMPLE
    Get-WUSetting

    ComputerName WindowsUpdates
    ------------ --------------
    PC1          Notify before download

    #>

    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory = $true,
                   Position = 1)]
        [ValidateCount(1,32)]
        [string[]]$ComputerName
    )


    [string]$ComputerName = $ComputerName.ToUpper()
    [string]$IsUnreadable = 'Unreadable'

    Write-Verbose "Assigning registry subkey path into memory."
    $RegKeyPath = 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update'

    Write-Verbose "Subkey is $RegKeyPath"


	try {

		$AUOption = (Get-ItemProperty $RegKeyPath -name 'AUOptions' -ErrorAction Stop).AUOptions
        Write-Verbose "Subkey value is $AUOption."
					
		
        Write-Verbose "Initializing switch() to query value $AUOption."

        $WUSetting = switch ($AUOption) {
			0 { 'Not configured' }
			1 { 'Never check for updates' }
			2 { 'Notify before download' }
			3 { 'Automatically download and notify of installation' }
			4 { 'Automatically download and schedule installation' }
			5 { 'Automatic Updates is required and users can configure it' }
			default { 'Unknown' } }

        Write-Verbose "switch() value refers to ""$WUSetting""."


    } catch {

        $WUSetting = $IsUnreadable

        Write-Verbose "Failed to locate the registry subkey and/or value in ""$RegKeyPath"" on $ComputerName."

        Break

    }
    

    [string]$ObjAboutComputer =  "WUObj for $ComputerName."
    
    try {

        Write-Verbose "$vm_CreateHashtable $ObjAboutComputer"

        $props = [Ordered]@{
			    "ComputerName" = $ComputerName.ToUpper();
			    "WindowsUpdates" = ($WUSetting | Out-String).Trim() }
		
        Write-Verbose "$vm_InitiateInstanceOfNewObject $ObjAboutComputer"
        
        $splat = @{'TypeName' = 'PSObject';
                   'Property' = $props;
                   'ErrorAction' = 'SilentlyContinue'}

        $WUObj = New-Object @splat

        Write-Verbose "$vm_ReturnObjOutput $ObjAboutComputer"

        Write-Output $WUObj

    } catch {

        Write-Verbose "$vm_ErrorInitiateInstanceOfNewObject $ObjAboutComputer"

    }
    
} # End Get-WUSetting function