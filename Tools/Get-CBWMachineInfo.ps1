﻿Function Get-CBWMachineInfo {
    <#
    .SYNOPSIS
    Get machine information.
    
    .DESCRIPTION
    Gets machine information such as operating system, CPU architecture, and other hardware details.
    
    .PARAMETER ComputerName
    The computer name or IP address of a system.
    
    .PARAMETER ErrorLogFilePath
    The location where to save error information.
    
    .PARAMETER Protocol
    Wsman is the default protocol for remote sessions, and Dcom is used secondarily. If Wsman fails,
    Dcom will be automatically attempted. If Dcom is specified using this parameter, Wsman will be
    automatically attempted.

    If both protocols fail, this function will terminate.

    .PARAMETER Credential
    The credential authorized on the remote system(s).
    
    .EXAMPLE
    An example
    
    .NOTES
    General notes
    #>

    [CmdletBinding()]
    [OutputType([System.Management.Automation.PSCustomObject])]

    Param (
        [Parameter(Position = 0,
            Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateCount(1, 32)]
        [string[]]$ComputerName,

        [Parameter()]
        [string]$ErrorLogFilePath = 'C:\RemoteMachineInfo.log',

        [Parameter()]
        [ValidateSet('Wsman', 'Dcom')]
        [string]$Protocol = 'Wsman',

        [Parameter()]
        [pscredential]$Credential
    )


    BEGIN {

        Write-Verbose "Execution Metadata:"
        Write-Verbose "User = $($env:userdomain)\$($env:USERNAME)"

        $id = [System.Security.Principal.WindowsIdentity]::GetCurrent()
        $IsAdmin = [System.Security.Principal.WindowsPrincipal]::new($id).IsInRole('Administrators')

        Write-Verbose "Is Admin = $IsAdmin"
        Write-Verbose "Computername = $env:COMPUTERNAME"
        Write-Verbose "OS = $((Get-CimInstance -ClassName Win32_Operatingsystem).Caption)"
        Write-Verbose "Host = $($host.Name)"
        Write-Verbose "PSVersion = $($PSVersionTable.PSVersion)"
        Write-Verbose "Runtime = $(Get-Date)"

        Write-Verbose "[BEGIN    ] Starting: $($MyInvocation.MyCommand)"

    } # BEGIN


    PROCESS {

        foreach ($Computer in $ComputerName) {

            $Computer = $Computer.ToUpper()

            # Count for protocol use (Wsman or Dcom)
            $Count = 0

            # Do/Until loop used for switching protocol use. If
            # one protocol fails, the other protocol is attempted.
            # If both have failed, the script terminates.
            do {

                Write-Verbose "[PROCESS  ] $($Computer): Connecting over $Protocol."

                try {

                    # Setup remote session
                    $option = New-CimSessionOption -Protocol $Protocol
                    
                    $session_params = @{
                        'ComputerName'  = $Computer;
                        'Credential'    = $Credential;
                        'SessionOption' = $option;
                        'ErrorAction'   = 'Stop' 
                    }

                    $session = New-CimSession @session_params #-OperationTimeoutSec 2


                    Write-Verbose "[PROCESS  ] $($Computer): Running query."

                    # Operating system
                    $os_params = @{
                        'CimSession' = $session;
                        'ClassName'  = 'CIM_OperatingSystem' 
                    }

                    $os = Get-CimInstance @os_params | 
                        Select-Object -Property Caption, ProductType, ServicePackMajorVersion, Version

                    $os_ProdType = switch ($os.ProductType) {
                        1 { 'Client' }
                        2 { 'Domain Controller' }
                        3 { 'Member Server' }
                        default { 'Unknown' } 
                    }
        
                    
                    # Hardware
                    $cs_params = @{
                        'CimSession'  = $session;
                        'ClassName'   = 'Win32_ComputerSystem';
                        'ErrorAction' = 'Stop' 
                    }
        
                    $cs = Get-CimInstance @cs_params |
                        Select-Object -Property Manufacturer, Model, NumberOfProcessors, NumberOfLogicalProcessors


                    # Physical memory
                    $pm_params = @{
                        'CimSession'  = $session;
                        'ClassName'   = 'CIM_PhysicalMemory';
                        'ErrorAction' = 'Stop' 
                    }
        
                    $pm = (Get-CimInstance @pm_params |
                        Select-Object -Property Capacity |
                        Measure-Object -Property Capacity -Sum).Sum
                        

                    # BIOS
                    $bios_params = @{
                        'CimSession' = $session;
                        'ClassName'  = 'CIM_BIOSElement' 
                    }

                    $bios = Get-CimInstance @bios_params | Select-Object -Property Manufacturer, Name, SerialNumber

                    
                    # CPU description
                    $proc_params = @{
                        'CimSession' = $session;
                        'ClassName'  = 'CIM_Processor' 
                    }

                    
                    # CPU architecture
                    $proc = Get-CimInstance @proc_params | Select-Object -Property Name, NumberOfCores, AddressWidth

        
                    # Remove remote session
                    Write-Verbose "[PROCESS  ] $($Computer): Closing session."
                    $session | Remove-CimSession


                    # Build output object
                    Write-Verbose "[PROCESS  ] $($Computer): Generating output."

                    $SysInfoObj = [pscustomobject]@{
                        'ComputerName'       = $Computer;
                        'OperatingSystem'    = $os.Caption;
                        'SPVersion'          = $os.ServicePackMajorVersion;
                        'OSVersion'          = $os.Version;
                        'ProductType'        = $os_ProdType;
                        'Manufacturer'       = $cs.Manufacturer;
                        'Model'              = $cs.Model;
                        'BIOS'               = $bios.Name;
                        'SerialNumber'       = $bios.SerialNumber;
                        'PhysicalProcessors' = $cs.NumberOfProcessors
                        'ProcessorName'      = $proc.Name;
                        'Cores'              = $proc.NumberOfCores;
                        'LogicalProcessors'  = $cs.NumberOfLogicalProcessors;
                        'Architecture'       = $proc[0].AddressWidth;
                        'InstalledMemory'    = $pm 
                    }

                    Write-Verbose "[PROCESS  ] $($Computer): Complete."
                    
                    Write-Output $SysInfoObj

                    # Remote session was successful on first or second attempt
                    # of protocol use. Set count to 2 to prevent loop executing.
                    $Count = 2

                }
                catch {

                    # Increment count by one to determine the attempt of protocol use.
                    $Count += 1
            
                    switch ($Protocol) {
                        
                        'Wsman' {
                            Write-Verbose "[PROCESS  ] $($Computer): $Protocol failed."
                            $PreviousProtocol = 'Wsman'
                            $Protocol = 'Dcom'
                        } # Wsman

                        'Dcom' {
                            Write-Verbose "[PROCESS  ] $($Computer): $Protocol failed."
                            $PreviousProtocol = 'Dcom'
                            $Protocol = 'Wsman'
                        } # dcom

                    } #switch

                
                    if ($PSBoundParameters.ContainsKey('ErrorLogFilePath')) {

                        Write-Verbose "[PROCESS  ] $($Computer): $PreviousProtocol results logged to $ErrorLogFilePath."

                        $Exception = ($Error[0].Exception.ErrorData.Message)
                        $ExceptionTime = Get-Date -f "yyyy-MM-dd HH:mm:ss"

                        $t = "[$($ExceptionTime)] $($Computer):`t$($Exception)"
                        $t | Out-file $ErrorLogFilePath -Append

                    }
                    else {

                        Write-Verbose "[PROCESS  ] $($Computer): $PreviousProtocol results not logged (error log not specified)."

                    } # if logging

                } # try/catch

            } until ($Count -eq 2)

        } # foreach computer

    } # PROCESS

    END {
    
        Write-Verbose "[END      ] Ending: $($MyInvocation.MyCommand)"
    
    } # END

} # function