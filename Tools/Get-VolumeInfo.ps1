﻿Function Get-VolumeInfo {
    <#

    .SYNOPSIS
    Gets all disk volumes from local or remote computers. This
    cmdlet supports Windows 7/Server 2008 SP2 and later.

    .DESCRIPTION
    This cmdlet can query local or remote computers for all
    disk volumes.

    .PARAMETER ComputerName
    The ComputerName parameter can accept from 1 to 32 computers, separated by a comma.

    .EXAMPLE
    Get-VolumeInfo -ComputerName CL1 | Format-Table

    ComputerName DriveLetter Label           Capacity  FileSystem BootVolume
    ------------ ----------- -----           --------  ---------- ----------
    CL1                      System Reserved 0.34 GB   NTFS            False
    CL1          F:          Data            929.87 GB NTFS            False
    CL1          C:                          930.73 GB NTFS             True
    CL1                                      0.44 GB   NTFS            False
    CL1          D:                          0 GB
    CL1                      Data            929.87 GB NTFS            False

    .EXAMPLE
    Get-VolumeInfo CL1,CL2,DC01 | Format-Table

    ComputerName DriveLetter Label               Capacity  FileSystem BootVolume
    ------------ ----------- -----               --------  ---------- ----------
    CL1                      System Reserved     0.34 GB   NTFS            False
    CL1          F:          Data                929.87 GB NTFS            False
    CL1          C:                              930.73 GB NTFS             True
    CL1                                          0.44 GB   NTFS            False
    CL1          D:                              0 GB
    CL1                      Data                929.87 GB NTFS            False
    CL2                      System Reserved     0.34 GB   NTFS            False
    CL2          C:                              930.73 GB NTFS             True
    CL2                                          0.44 GB   NTFS            False
    CL2          D:          DVD Video Recording 3.15 GB   UDF             False
    DC01                     System Reserved     0.34 GB   NTFS            False
    DC01         C:                              79.66 GB  NTFS             True
    DC01         D:                              0 GB

    .EXAMPLE
    Get-VolumeInfo CL1,CL2,CL3 | Format-Table -GroupBy ComputerName

    ComputerName: CL1

    ComputerName DriveLetter Label    Capacity  FileSystem BootVolume
    ------------ ----------- -----    --------  ---------- ----------
    CL1          C:          BOOTCAMP 195.14 GB NTFS             True


    ComputerName: CL2

    ComputerName DriveLetter Label           Capacity  FileSystem BootVolume
    ------------ ----------- -----           --------  ---------- ----------
    CL2                      System Reserved 0.34 GB   NTFS            False
    CL2          C:                          930.73 GB NTFS             True
    CL2                                      0.44 GB   NTFS            False
    CL2          D:                          0 GB


    ComputerName: CL3

    ComputerName DriveLetter Label             Capacity  FileSystem BootVolume
    ------------ ----------- -----             --------  ---------- ----------
    CL3          C:          Acer              898.27 GB NTFS             True
    CL3                      Recovery          0.59 GB   NTFS            False
    CL3                      Push Button Reset 32.24 GB  NTFS            False
    CL3          D:                            0 GB

    #>

[CmdletBinding()]
	param (
		[Parameter(Mandatory = $true,
                   Position = 1)]
		[ValidateCount(1,32)]
		[string[]]$ComputerName
	)

	
    Write-Verbose "$vm_EnumerateComputers $($ComputerName -join ', ')."

	foreach ($Computer in $ComputerName) {

        [string]$ObjAboutComputer = "VolumeInfo for $Computer."

        try {

            [string]$ClassName = 'Win32_Volume'

            Write-Verbose "Retrieving properties and values from class $ClassName on $ComputerName."

            $splat = @{'Namespace' = 'ROOT\CIMv2';
                       'ComputerName' = $Computer;
                       'ClassName' = $ClassName;
                       'ErrorAction' = 'SilentlyContinue'}
		    
            $VolumeInfo = Get-CimInstance @splat |
            Select-Object @{ Name='ComputerName'; Expression={ $computer.ToUpper() } },
                            DriveLetter,
                            Label,
                            @{ Name='Capacity'; Expression={ "$([math]::round(($_.Capacity/1GB), 2)) GB" }},
                            FileSystem,
                            BootVolume

            Write-Verbose "$vm_ReturnObjOutput $ObjAboutComputer"
            
            Write-Output $VolumeInfo

        } catch {

            Write-Verbose "$vm_ErrorInitiateInstanceOfNewObject $ObjAboutComputer"
        }

	} # foreach computer

} # End Get-VolumeInfo function