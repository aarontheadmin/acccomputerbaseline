﻿#Needs foreach broken out for each type.
Function Get-InstalledSecurityProduct {
    <#

    .SYNOPSIS
    Obtains installed firewall, antivirus, and antispyware software.

    .DESCRIPTION
    This cmdlet obtains all instances of installed firewall, antivirus, 
    and antispyware software on specified computers.

    .PARAMETER ComputerName
    The ComputerName parameter can accept from 1 to 32 computers, separated by a comma.

    .EXAMPLE
    Get-InstalledSecurityProducts -ComputerName DOLP-PC

    ComputerName  : DOLP-PC
    ProductType   : AntiVirusProduct
    Software      : Windows Defender
    SignedExePath : %ProgramFiles%\Windows Defender\MSASCui.exe
    TimeStamp     : Thu, 29 Dec 2016 16:47:54 GMT

    ComputerName  : DOLP-PC
    ProductType   : AntiSpywareProduct
    Software      : Windows Defender
    SignedExePath : %ProgramFiles%\Windows Defender\MSASCui.exe
    TimeStamp     : Thu, 29 Dec 2016 16:56:07 GMT


    This is an example of executing the cmdlet against one computer.

    .EXAMPLE
    Get-InstalledSecurityProducts DOLP-PC,WAGO-PC,VEDD-PC

    ComputerName ProductType        Software         SignedExePath                               TimeStamp                    
    ------------ -----------        --------         -------------                               ---------                    
    DOLP-PC      AntiVirusProduct   Windows Defender %ProgramFiles%\Windows Defender\MSASCui.exe Thu, 29 Dec 2016 16:47:54 GMT
    DOLP-PC      AntiSpywareProduct Windows Defender %ProgramFiles%\Windows Defender\MSASCui.exe Thu, 29 Dec 2016 16:56:07 GMT
    WAGO-PC      AntiVirusProduct   Windows Defender %ProgramFiles%\Windows Defender\MSASCui.exe Wed, 14 Dec 2016 20:36:12 GMT
    WAGO-PC      AntiSpywareProduct Windows Defender %ProgramFiles%\Windows Defender\MSASCui.exe Thu, 29 Dec 2016 22:41:57 GMT
    VEDD-PC      AntiVirusProduct   Windows Defender %ProgramFiles%\Windows Defender\MSASCui.exe Thu, 15 Dec 2016 06:04:14 GMT
    VEDD-PC      AntiSpywareProduct Windows Defender %ProgramFiles%\Windows Defender\MSASCui.exe Wed, 28 Dec 2016 11:57:16 GMT


    This is an example of executing the cmdlet against multiple remote computers.

    #>

	[CmdletBinding()]
	Param
    (
		[Parameter(Mandatory = $true, Position = 1)]
		[ValidateCount(1,32)]
		[string[]]$ComputerName,

        [Parameter(Mandatory = $true)]
        [ValidateSet('AntiSpywareProduct',
                     'AntiVirusProduct',
                     'FirewallProduct')]
        [string]$ProductType
	)

	
    Write-Verbose "$vm_EnumerateComputers $($ComputerName -join ', ')."

	foreach ($Computer in $ComputerName) {

        [string]$ObjAboutComputer = "for $Computer."

		try {
			
            Write-Verbose "Accessing class $ProductType."

            $splat = @{'Namespace' = 'ROOT\SecurityCenter2';
                       'ClassName' = $ProductType;
                       'ComputerName' = $Computer;
                       'ErrorAction' = 'SilentlyContinue' }


            $List = Get-CimInstance @splat
                    
            Write-Verbose "Listed values in class $ProductType."

        } catch {

            Write-Verbose "Failed to access class $ProductType."

        }

		foreach ($item in $List) {

            Write-Verbose "$vm_CreateHashtable $type $ObjAboutComputer"

			$props = [Ordered]@{
                    ComputerName  = $Computer.ToUpper();
                    ProductType   = $ProductType;
                    Software      = $item.displayName;
                    SignedExePath = $item.pathToSignedProductExe;
                    TimeStamp     = $item.TimeStamp
            }

            Write-Verbose "Creating object out of $ProductType $ObjAboutComputer"

            try {
                    Write-Verbose "$vm_InitiateInstanceOfNewObject $ProductType $ObjAboutComputer"

					$InstalledSecProdsObj = New-Object -TypeName PSObject -Property $props -ErrorAction SilentlyContinue
                    
                    Write-Verbose "$vm_ReturnObjOutput $ObjAboutComputer"
                            
                    Write-Output $InstalledSecProdsObj
                    
            } catch {

                Write-Verbose "$vm_ErrorInitiateInstanceOfNewObject $item $ObjAboutComputer"

            }

		}

	} # End foreach $Computer

} # End Get-InstalledSecurityProducts function