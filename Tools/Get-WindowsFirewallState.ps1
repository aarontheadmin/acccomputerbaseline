﻿Function Get-WindowsFirewallState {
    <#

    .SYNOPSIS
    Obtains Windows Firewall settings of local or remote computers.

    .DESCRIPTION
    This function obtains the status of each firewall profile in Windows Firewall 
    for local or remote computers. Netsh is used to query both systems;
    any time a local computer is specified in the -ComputerName parameter,
    netsh is executed locally on it; Invoke-Command is used to execute the
    query against remote computers.

    .EXAMPLE
    Get-WindowsFirewallState -ComputerName SYSTEM-A

    ComputerName FirewallDomain FirewallPrivate FirewallPublic
    ------------ -------------- --------------- --------------
    SYSTEM-A     ON             ON              OFF

    This is an example of how to execute the query against the local computer, SYSTEM-A.

    .EXAMPLE
    Get-WindowsFirewallState -ComputerName SYSTEM-A, REMOTE-PC1

    ComputerName FirewallDomain FirewallPrivate FirewallPublic
    ------------ -------------- --------------- --------------
    SYSTEM-A     ON             ON              OFF
    REMOTE-PC1   ON             ON              ON

    This is an example of how to execute the cmdlet against a local computer (SYSTEM-A)
    and a remote computer (REMOTE-PC1).

    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true,
                   Position = 1)]
        [ValidateCount(1,32)]
        [string[]]$ComputerName
    )

    [string]$IsUnreadable = 'Unreadable'
    $ErrorActionPreference = "SilentlyContinue"


    try {
            $FirewallBlock = {

                    $netshcmd = netsh advfirewall show allprofiles
                        
                        
                    if ($DomainProfileCmd = $netshcmd | Select-String 'Domain Profile' -Context 2 | Out-String)
                    {   
                        $DomainProfile = ($DomainProfileCmd.Substring($DomainProfileCmd.Length - 9)).Trim()
                    } else {
                        $DomainProfile = $IsUnreadable
                    }

                        
                    if ($PrivateProfileCmd = $netshcmd | Select-String 'Private Profile' -Context 2 | Out-String)
                    {
                        $PrivateProfile = ($PrivateProfileCmd.Substring($PrivateProfileCmd.Length - 9)).Trim()
                    } else {
                        $PrivateProfile = $IsUnreadable
                    }

                        
                    if ($PublicProfileCmd = $netshcmd | Select-String 'Public Profile' -Context 2 | Out-String)
                    {
                        $PublicProfile = ($PublicProfileCmd.Substring($PublicProfileCmd.Length - 9)).Trim()
                    } else {
                        $PublicProfile = $IsUnreadable
                    }

                        
                    $FirewallObject = New-Object -TypeName PSObject -ErrorAction SilentlyContinue
                    Add-Member -inputObject $FirewallObject -memberType NoteProperty -name "FirewallDomain" -value $DomainProfile
                    Add-Member -inputObject $FirewallObject -memberType NoteProperty -name "FirewallPrivate" -value $PrivateProfile
                    Add-Member -inputObject $FirewallObject -memberType NoteProperty -name "FirewallPublic" -value $PublicProfile
                        
                    Write-Output $FirewallObject
            }


            #if (($env:COMPUTERNAME -match $ComputerName)) {

             #   $Command = & $FirewallBlock

            #} else {

                $splat = @{'ComputerName' = $ComputerName;
                           'ScriptBlock' = $FirewallBlock;
                           'ErrorAction' = 'SilentlyContinue'}

                $Command = Invoke-Command @splat
            #}

            $Command | Select-Object -Property `
                            @{ n = 'ComputerName'; e = { $_.PSComputerName.ToUpper() } },

                                    #}
                                #}
                            #},
                            FirewallDomain,
                            FirewallPrivate,
                            FirewallPublic
 
    } catch {
    
           Write-Warning ($_.Exception.Message -split ' For')[0]
    }

}