﻿Function Get-PsExecutionPolicy {
    <#

    .SYNOPSIS
    Returns the Execution Policy of the local computer.

    .DESCRIPTION
    This function can only be used locally and not on remote systems.

    .EXAMPLE
    Get-PsExecutionPolicy

            Scope ExecutionPolicy
            ----- ---------------
    MachinePolicy       Undefined
       UserPolicy       Undefined
          Process       Undefined
      CurrentUser       Undefined
     LocalMachine    RemoteSigned

    #>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true,
                   Position = 1)]
        [string[]]$ComputerName
    )


    try {

        Write-Verbose "Retrieving Execution Policy on $($ComputerName)."

        $ExecutionPolicy = Get-ExecutionPolicy -List -ErrorAction SilentlyContinue

        Write-Verbose "$vm_ReturnObjOutput for $ComputerName."

        Write-Output $ExecutionPolicy
    
    } catch {

        Write-Verbose "Failed to retrieve Execution Policy on $($ComputerName)."

    }

} # End Get-PsConfiguration function