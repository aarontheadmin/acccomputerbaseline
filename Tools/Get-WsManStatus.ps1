﻿Function Get-WsManStatus {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true,
                    Position = 1)]
        [ValidateCount(1,32)]
        [string[]]$ComputerName
    )


    BEGIN {}



    PROCESS {


        $results = foreach ($Computer in $ComputerName) {

            $Computer = $Computer.ToUpper()


            try {

                Write-Verbose ("Validating WsMan functionality enabled on {0}" -f  $Computer)

                $test = Test-WSMan -ComputerName $Computer -ErrorAction Stop


                Write-Verbose ("WSMan working on {0}" -f  $Computer)

                $props = @{
                    ComputerName = $Computer
                    Status = "PASS"
                    Message = $test
                }

            } catch {

                Write-Verbose ("WSMan test failed on {0}" -f  $Computer)

                $props = @{
                    ComputerName = $Computer
                    Status = "FAIL"
                    Message = $_
                }

            } #try wsman


            New-Object -TypeName PSObject -Property $props


        } # foreach computer


    } # Process


    END {

        Write-Output $results

    }


} # function Get-WsManStatus