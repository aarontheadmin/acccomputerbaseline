﻿Function Get-NetworkConfiguration {
    <#

    .SYNOPSIS
    Gets network adapter configurations from local or remote
    computers. This cmdlet supports Windows 7/Server 2008 SP2 and
    later.

    .DESCRIPTION
    This cmdlet can query local or remote computers for enabled
    network adapters. If the operating system is equal to or
    greater than Windows 8.1 or Windows Server 2012 R2, RSS, 
    VMQ, bandwidth, and other settings are included in the query.
    This verification is completed by calling the function:

        Get-MinimumOSVersion -ComputerName CL1

    Operating systems older than Windows 8.1 or Windows Server
    2012 R2 return objects without the advanced properties (VMQ,
    bandwidth, etc.)

    .PARAMETER ComputerName
    The ComputerName parameter can accept from 1 to 32 computers, separated by a comma.

    .EXAMPLE
    Get-NetworkConfiguration -ComputerName CL1

    ComputerName          : CL1
    NetConnectionID       : Ethernet 2
    ifIndex               : 10
    Description           : Intel(R) PRO/1000 GT Desktop Adapter
    MACAddress            : 00:11:22:33:44:55
    IPAddress             :
    IPSubnet              :
    DefaultIPGateway      :
    DNSServerSearchOrder  :
    DHCPEnabled           : True
    DNSHostName           :
    DriverVersion         : 8.4.13.0
    LinkSpeed             : 1 Gbps
    NdisVersion           : 6.0
    DeviceID              : {83311060-B827-4429-9BCE-AB1B0A689B4C}
    VMQueueingEnabled     :
    RSSEnabled            :
    NumberOfReceiveQueues :
    MaxProcessors         :

    ComputerName          : CL1
    NetConnectionID       : Ethernet
    ifIndex               : 2
    Description           : Realtek PCIe GBE Family Controller
    MACAddress            : BC:EE:AA:BB:CC:DD
    IPAddress             : 10.0.1.12, fe80::c4f8:da7a:c486:4f1d
    IPSubnet              : 255.255.0.0, 64
    DefaultIPGateway      : 10.0.0.1
    DNSServerSearchOrder  : 10.0.1.8, 10.0.1.9
    DHCPEnabled           : False
    DNSHostName           : CL1
    DriverVersion         : 9.1.404.2015
    LinkSpeed             : 1 Gbps
    NdisVersion           : 6.40
    DeviceID              : {15825E30-2E65-48FF-959E-87107BD8E9BC}
    VMQueueingEnabled     :
    RSSEnabled            :
    NumberOfReceiveQueues :
    MaxProcessors         :

    .EXAMPLE
    Get-NetworkConfiguration CL1,CL2,DC01 |
    Select-Object -Property ComputerName,NetConnectionID,ifIndex,
                            IPAddress,IPSubnet,DefaultIPGateway,
                            DNSServerSearchOrder |
    Format-Table -AutoSize

    ComputerName NetConnectionID             ifIndex IPAddress                             IPSubnet        DefaultIPGateway DNSServerSearchOrde
    ------------ ---------------             ------- ---------                             --------        ---------------- -------------------
    CL1          Ethernet 2                  10
    CL1          Ethernet                    2       10.0.1.12, fe80::c4f8:da7a:c486:4f1d  255.255.0.0, 64 10.0.12.1         10.0.12.8, 10.0.12.9
    CL1          vEthernet (CBWOFFICE.LOCAL) 12      10.0.3.138, fe80::648b:f19f:c131:a1ab 255.255.0.0, 64 10.0.12.1         10.0.12.8, 10.0.12.9
    CL2          Local Area Connection       5       10.0.3.5, fe80::d0f6:3a04:8a2:4cc8    255.255.0.0, 64 10.0.12.1         10.0.12.8, 10.0.12.9
    DC01         Ethernet 3                  13      10.0.1.8, fe80::38f2:146a:585f:6e74   255.255.0.0, 64 10.0.12.1         10.0.12.16, 10.0.12.8

    #>

	[CmdletBinding()]
	Param
    (
		[Parameter(Mandatory = $true,
                   Position = 1)]
		[ValidateCount(1,32)]
		[string[]]$ComputerName
	)
    
    [string]$Namespace = 'ROOT\CIMv2'


    Write-Verbose "$vm_EnumerateComputers $($ComputerName.ToUpper() -join ', ')."

	foreach ($Computer in $ComputerName) {

        [string]$Computer = $Computer.ToUpper()
        [string]$ObjAboutComputer = "for $Computer."

        try {

            [string]$ClassName = 'Win32_NetworkAdapter'

            Write-Verbose "Loading class $ClassName on $Computer)."

            $splat = @{'Namespace' = $Namespace;
                       'ClassName' = $ClassName;
                       'Filter' = "NetEnabled=$True";
                       'ComputerName' = $Computer;
                       'ErrorAction' = 'SilentlyContinue'}

    	    $EnabledNetAdapters = Get-CimInstance @splat |
                                  Select-Object -Property InterfaceIndex,
                                                          NetConnectionID

            Write-Verbose "Enumerating enabled network adapters on $Computer."

            foreach ($Adapter in $EnabledNetAdapters) {

                $ifIndex = $Adapter.InterfaceIndex

                Write-Verbose "Adapter ifIndex is $ifIndex."

                Write-Verbose "Loading adapter configuration for ifIndex $ifIndex."

                $splat = @{'Namespace' = $NameSpace;
                           'ClassName' = 'Win32_NetworkAdapterConfiguration';
                           'ComputerName' = $Computer;
                           'Filter' = "InterfaceIndex='$([uint32]$ifIndex)'";
                           'ErrorAction' = 'SilentlyContinue'}

                $NetAdapterConfig = Get-CimInstance @splat |
                                    Select-Object -Property Description,
                                                            DefaultIPGateway,
                                                            DHCPEnabled,
                                                            DNSHostName,
                                                            DNSServerSearchOrder,
                                                            IPAddress,
                                                            IPSubnet,
                                                            InterfaceIndex,
                                                            MACAddress,
                                                            SettingID

                Write-Verbose "$vm_CreateHashtable ifIndex $ifIndex $ObjAboutComputer"
                
                $props = [ordered]@{
                            "ComputerName" = $Computer;
                            "NetConnectionID" = $Adapter.NetConnectionID;
                            "ifIndex" = $ifIndex;
                            "Description" = $NetAdapterConfig.Description;
                            "MACAddress" = $NetAdapterConfig.MACAddress;
                            "IPAddress" = $NetAdapterConfig.IPAddress -join ", ";
                            "IPSubnet" = $NetAdapterConfig.IPSubnet -join ", ";
                            "DefaultIPGateway" = $NetAdapterConfig.DefaultIPGateway -join ", ";
                            "DNSServerSearchOrder" = $NetAdapterConfig.DNSServerSearchOrder -join ", ";
                            "DHCPEnabled" = $NetAdapterConfig.DHCPEnabled;
                            "DNSHostName" = $NetAdapterConfig.DNSHostName }

                $SettingID = $NetAdapterConfig.SettingID
                $ProductVersions = Get-MinimumOSVersion -ComputerName $computer -ErrorAction SilentlyContinue
                $ComputerOSVersion = [version]$ProductVersions.ComputerOSVersion
                $MinimumOSVersionByProductType = [version]$ProductVersions.MinimumOSVersionByProductType

                
                #Write-Verbose "Computer OS version on $($Computer.Trim().ToUpper()) is $ComputerOSVersion."

                if ($ComputerOSVersion -ge $MinimumOSVersionByProductType) {

                    Write-Verbose "Minimum support OS for advanced cmdlets: $MinimumOSVersionByProductType."

                    try {
                            
                            Write-Verbose "Loading NetAdapter properties for ifIndex $ifIndex."

                            $splat = @{'CimSession' = $Computer;
                                       'InterfaceIndex' = $ifIndex;
                                       'ErrorAction' = 'SilentlyContinue'}

                            $NetAdapters = Get-NetAdapter @splat | 
                                        Select-Object -Property ifAlias,
                                                                Name,
                                                                DriverVersion,
                                                                LinkSpeed,
                                                                NdisVersion,
                                                                DeviceID,
                                                                InstanceId
                    
                    } catch {

                            Write-Verbose "Failed load NetAdapter properties for ifIndex $ifIndex."

                    }


                    try {
                            
                            Write-Verbose "Loading NetAdapterVmq properties for ifIndex $ifIndex."

                            $NetAdapterVmq = Get-NetAdapterVmq -CimSession $Computer -ErrorAction SilentlyContinue |
                                             Select-Object -Property Enabled,
                                                                     InstanceId |
                                            Where-Object { $_.InstanceId -match $SettingID }
                    
                    } catch {

                            Write-Verbose "Failed load NetAdapterVmq properties for ifIndex $ifIndex."
                        
                    }


                    try {

                            Write-Verbose "Loading NetAdapterRss properties for ifIndex $ifIndex."
                
                            $NetAdapterRss = Get-NetAdapterRss -CimSession $Computer -ErrorAction SilentlyContinue |
                                            Select-Object Enabled,
                                                          InstanceId,
                                                          NumberOfReceiveQueues,
                                                          MaxProcessors |
                                            Where-Object { $_.InstanceId -match $SettingID }
                    
                    } catch {

                        Write-Verbose "Failed load NetAdapterRss properties for ifIndex $ifIndex."

                    }

            
                    Write-Verbose "Appending advanced properties and values to hashtable for object $ObjAboutComputer"
                
                    $props += [ordered]@{
                                    "DriverVersion" = $NetAdapters.DriverVersion;
                                    "LinkSpeed" = $NetAdapters.LinkSpeed;
                                    "NdisVersion" = $NetAdapters.NdisVersion;
                                    "DeviceID" = $NetAdapters.DeviceID;
                                    "VMQueueingEnabled" = $NetAdapterVmq.Enabled;
                                    "RSSEnabled" = $NetAdapterRss.Enabled;
                                    "NumberOfReceiveQueues" = $NetAdapterRss.NumberOfReceiveQueues;
                                    "MaxProcessors" = $NetAdapterRss.MaxProcessors
                    }

                } else {

                    Write-Verbose "Not appending advanced properties and values to hashtable for object for ifIndex $ifIndex (doesn't exist)."

                } # End if/else

                
                try {
                        Write-Verbose "$vm_InitiateInstanceOfNewObject $ObjAboutComputer"

                        $NetConfigObj = New-Object -TypeName PSObject -Property $props -ErrorAction SilentlyContinue
                        
                        Write-Verbose "$vm_ReturnObjOutput $ObjAboutComputer"

                        Write-Output $NetConfigObj
                
                } catch {

                    Write-Verbose "$vm_ErrorInitiateInstanceOfNewObject $ObjAboutComputer"

                }
                
            } # End foreach adapter

        } catch {

            Write-Verbose "Failed to load NetAdapter configuration for ifIndex $($ifIndex) $ObjAboutComputer"

        } # End try/catch

    } # End foreach computer
		    
} # End Get-NetworkConfiguration function