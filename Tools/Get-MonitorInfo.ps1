﻿Function Get-MonitorInfo {
    <#

    .SYNOPSIS
    Gets monitor information from local or remote computers.
    This cmdlet supports Windows 7/Server 2008 SP2 and later.

    .DESCRIPTION
    This cmdlet can query local or remote computers for all
    active monitors and their respecitve UserFriendlyName, Serial
    Number, Manufacturer Name, and Product Code ID. This cmdlet
    only returns exactly what the manufacturer has specified in
    the monitor firmware.

    .PARAMETER ComputerName
    The ComputerName parameter can accept from 1 to 32 computers, separated by a comma.

    .EXAMPLE
    Get-MonitorInfo -ComputerName PC-102

    ComputerName     : PC-102
    UserFriendlyName : HP 23tm
    SerialNumberID   : CNC39999N9
    ManufacturerName : HWP
    ProductCodeID    : 310F



    This is an example of executing the cmdlet against one computer.

    .EXAMPLE
    Get-MonitorInfo -ComputerName CL1,PC-102 | Format-Table

    ComputerName   UserFriendlyName SerialNumberID   ManufacturerName ProductCodeID   
    ------------   ---------------- --------------   ---------------- -------------   
    CL1            Color LCD        0                APP              A00E            
    PC-102         HP 23tm          CNC39999N9       HWP              310F



    This is an example of executing the cmdlet against multiple computers and
    formatting the output to a table for better legibility.

    #>

    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true, Position = 1)]
        [ValidateCount(1,32)]
        [string[]]$ComputerName
    )

    [string]$Namespace = 'ROOT\WMI'
    [string]$ClassName = 'WMIMonitorID'


    Write-Verbose "$vm_EnumerateComputers $($ComputerName.ToUpper() -join ', ')."

    foreach ($Computer in $ComputerName) {

        [string]$Computer = $Computer.ToUpper()
        [string]$ObjAboutComputer = "for $Computer."

        try {

            [string]$Namespace = 'ROOT\WMI'
            [string]$ClassName = 'WMIMonitorID'


            Write-Verbose "Setting up $Namespace\$ClassName on $Computer."

            $splat = @{'Namespace' = $Namespace;
                       'ClassName' = $ClassName;
                       'ComputerName' = $Computer;
                       'ErrorAction' = 'SilentlyContinue'}

            $ActiveMonitors = Get-CimInstance @splat

            Write-Debug "Setup of $Namespace\$ClassName on $Computer completed."

        } catch {

            Write-Verbose "Failed to access $Namespace\$ClassName on $Computer."

        } # End try/catch

        
        Write-Verbose "Enumerating available monitors on $Computer."

        foreach ($monitor in $ActiveMonitors) {

            Write-Debug "Trying to create hashtable with monitors."

            Write-Verbose "$vm_CreateHashtable $ObjAboutComputer"

            $props = [ordered]@{
                
                    ComputerName     = $Computer;

                    UserFriendlyName = ($monitor |
                                            Select-Object -ExpandProperty UserFriendlyName |
                                            ForEach-Object { [char]$_ }) -join '';
                    SerialNumberID   = ($monitor |
                                            Select-Object -ExpandProperty SerialNumberID |
                                            ForEach-Object { [char]$_ }) -join '';
                    ManufacturerName = ($monitor |
                                            Select-Object -ExpandProperty ManufacturerName |
                                            ForEach-Object { [char]$_ }) -join '';
                    ProductCodeID    = ($monitor |
                                            Select-Object -ExpandProperty ProductCodeID |
                                            ForEach-Object { [char]$_ }) -join ''
            }

            
            Write-Verbose "$vm_InitiateInstanceOfNewObject $ObjAboutComputer"
            
            try {

                $splat = @{'TypeName' = 'PSObject';
                           'Property' = $props;
                           'ErrorAction' = 'SilentlyContinue'}

                $MonitorInfoObj = New-Object @splat
                
                Write-Verbose "$vm_ReturnObjOutput $ObjAboutComputer"

                Write-Output $MonitorInfoObj

            } catch {

                Write-Verbose "$vm_ErrorInitiateInstanceOfNewObject $ObjAboutComputer"

            } # End try/catch

        } # End foreach $monitor
    
    } # End foreach $Computer

} # End Get-MonitorInfo function