﻿# Replace null or whitespace with string 'N/A'.
Function IsEmpty {
	param
	(
		[Parameter()]
		$str
	)


    Write-Verbose "Checking if string is not empty or null."
	
	if (IsNullOrEmpty($str))
	{

        Write-Verbose "String is $str."

        Write-Verbose "$vm_ReturnObjOutput to pipeline."

        Write-Output $str

    } else {

        Write-Verbose "String is empty or null; populating ""N/A"" to string."

        Write-Verbose "$vm_ReturnObjOutput to pipeline."
    
        Write-Output 'N/A'
    
    }

} # End IsEmpty function