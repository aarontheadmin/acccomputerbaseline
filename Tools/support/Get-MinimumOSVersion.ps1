﻿Function Get-MinimumOSVersion {

    <#
    .SYNOPSIS
    Gets the minimum operating system version based on
    product type (client or server edition).

    .DESCRIPTION
    This cmdlet checks if a computer is a "Client"" OS or
    "Server"" OS, then identifies the minimum version of
    that type of OS that is required for PowerShell
    cmdlets that are supported only on those OS versions
    and newer. If they don't match or exceed, the cmdlet(s)
    will not be executed against that machine.
    
    For example, DC-A is running Windows Server 2012 R2
    (version 6.3). This cmdlet identifies the OS type as 
    "Server". Because the OS type is "Server", the minimum 
    OS version for "Server" is 6.3 ("Client" is 8.1). The
    server version is compared to the minimum required
    version. Because the versions match (or exceed), the
    cmdlet will be able to execute against the computer.

    The reason for this check is that some tools are able
    to collect information from all operating systems,
    but some operating systems don't have all the providers
    or classes available.

    An example is the Get-NetAdapterRss cmdlet. This cmdlet
    is not supported on Windows 7 or Windows Server 2008. But
    because this cmdlet is part of a tool that collects
    network adapter information from all operating systems,
    this check is done so that Get-NetAdapterRss is diverted 
    and its output is not returned for that particular network
    adapter.

    .Example

    Executing cmdlet on local computer.

    PS C:\>Get-MinimumOSVersion

    ComputerName ComputerOSVersion MinimumOSVersion
    ------------ ----------------- ----------------
    DOLPHIN      10.0.14393        8.1
    
    .Example

    Executing cmdlet on a remote computer.

    PS C:\> Get-MinimumOSVersion -ComputerName maple

    ComputerName ProductType ComputerOSVersion MinimumOSVersion
    ------------ ----------- ----------------- -----------------
    MAPLE        Client      10.0.14393        8.1
    
    
    #>
    [CmdletBinding()]
    Param (

        [Parameter(Mandatory = $true, Position = 1)]
        [ValidateCount(1,32)]
        [string[]]$ComputerName

    )


    Write-Verbose "$vm_EnumerateComputers $($ComputerName.Trim().ToUpper())."

    foreach ($computer in $ComputerName) {

        [string]$Computer = $Computer.ToString()
        [string]$ObjAboutComputer = "for $Computer."


        try {

            Write-Verbose "Initializing OSInfo object for $($Computer.Trim().ToUpper())."

            $OSTypeInfo = Get-OSInfo -ComputerName $Computer -ErrorAction Stop |
                          Select-Object RoleInNetwork,OSVersion

            Write-Verbose "Completed initializing OSInfo object."


        } catch {

            Write-Verbose "Failed to create OSInfo object(s) $ObjAboutComputer"

            Break

        }

        [string]$ComputerRole = $OSTypeInfo.RoleInNetwork
        
        Write-Verbose "Checking minimum OS version required for OS type: ""$ComputerRole""."

        if ($ComputerRole -match 'Client') {
            
            $ComputerOSVersion = $OSTypeInfo.OSVersion
            $MinimumOSVersionByProductType = '8.1'

            Write-Verbose "Minimum OS supported for ""$ComputerRole"": $MinimumOSVersionByProductType"
            Write-Verbose "($Computer) OS version: $ComputerOSVersion"

        } elseif ($ComputerRole -eq 'Domain Controller' -or 'Member Server') {
            
            Write-Verbose "Computer role is $ComputerRole."

            $ComputerOSVersion = $OSTypeInfo.OSVersion
            $MinimumOSVersionByProductType = '6.3'

            Write-Verbose "Minimum OS supported for ""$ComputerRole"": $MinimumOSVersionByProductType"
            Write-Verbose "($Computer) OS version: $ComputerOSVersion"

        } else {

            Write-Verbose "Computer role is unknown."
            
            $ComputerOSVersion = '0'
            $MinimumOSVersionByProductType = '0'

            Write-Verbose "($Computer) OS version: $ComputerOSVersion"
            Write-Verbose "($Computer) Minimum server OS supported: $MinimumOSVersionByProductType"

            Break

        } # End if $ComputerRole


        try {

            Write-Verbose "$vm_CreateHashtable OSTypeInfo $ObjAboutComputer"

	        $props = [Ordered]@{
                "ComputerName" = $Computer.ToUpper();
                "ProductType" = $ComputerRole;
		        "ComputerOSVersion" = $ComputerOSVersion;
		        "MinimumOSVersionByProductType" = $MinimumOSVersionByProductType }
        
            
            Write-Verbose "$vm_InitiateInstanceOfNewObject $ObjAboutComputer"
            
            $MinOSVersionXProdTypeObj = New-Object -TypeName PSObject -Property $props -ErrorAction Stop

            Write-Verbose "$vm_ReturnObjOutput $ObjAboutComputer"

            Write-Output $MinOSVersionXProdTypeObj

        } catch {

            Write-Verbose "Failed to create MinimumOSVersionByProductType object(s) $ObjAboutComputer"

            Break

        } # End try/catch

    } # foreach

} # End Get-MinimumOSVersion function



