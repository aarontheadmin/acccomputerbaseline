﻿# VERBOSE MESSAGES



Set-Variable -Name vm_EnumerateComputers `
             -Value "Enumerating computers:" `
             -Option Constant `
             -Description 'Used when enumerating computers (i.e. for-loop).'


Set-Variable -Name vm_AssignValuesToProperties `
             -Value 'Assigning class property values to variables.' `
             -Option Constant `
             -Description 'Used when assigning values to variables in a construct.'


Set-Variable -Name vm_FailAssignValuesToProperties `
             -Value 'Failed to assign class properties to variables.' `
             -Option Constant `
             -Description 'Used when assigning values to variables in a construct.'



Set-Variable -Name vm_CreateHashtable `
             -Value 'Creating hashtable for' `
             -Option Constant `
             -Description 'Used when assigning values and properties to a hashtable.'


Set-Variable -Name vm_FailWritePropsValsToNewObject `
             -Value 'Failed to write properties and values to new instance of object' `
             -Option Constant `
             -Description 'Used when assigning values and properties to a new instance of an object.'



Set-Variable -Name vm_InitiateInstanceOfNewObject `
             -Value 'Initiate new instance of object and assign hashtable' `
             -Option Constant `
             -Description 'Used when creating a new instance of an object.'


Set-Variable -Name vm_ErrorInitiateInstanceOfNewObject `
             -Value 'Error initiating new instance of object' `
             -Option Constant `
             -Description 'Used when creating a new instance of an object.'



Set-Variable -Name vm_ReturnObjOutput `
             -Value 'Returning output from instance of object' `
             -Option Constant `
             -Description 'Used to return output of a given object.'


Set-Variable -Name vm_ErrorCheck `
             -Value 'Checking for errors...' `
             -Option Constant `
             -Description 'Used to return indicate if errors were present while handling the object for output.'


Set-Variable -Name vm_ErrorReturn `
             -Value 'Returning error message.' `
             -Option Constant `
             -Description 'Used to return errors were found.'


Set-Variable -Name vm_ErrorNotFound `
             -Value 'No errors found.' `
             -Option Constant `
             -Description 'Used to return that no errors were found.'


Set-Variable -Name vm_ObjectDiscarded `
             -Value 'Object discarded from memory.' `
             -Option Constant `
             -Description 'Used to discard objects that failed to initialize.'



             
