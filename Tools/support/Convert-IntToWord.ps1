﻿Function Convert-IntToWord {
# Convert binary $true to 'Enabled, $false to 'Disabled'.

    [CmdletBinding()]
	Param
    (
		[Parameter (
            Position = 1, 
            Mandatory = $True,
            HelpMessage = 'Must be 0 or 1.')]
        [ValidateSet (0, 1)]
		[byte]$Integer
	)


    Write-Verbose "Initializing switch() to query value $Integer."
	
	$Word = switch ($Integer) {
                
                0 { 'Disabled' }
                1 { 'Enabled' }
                default { 'Unknown' }

            }

    Write-Verbose "switch() value refers to ""$Word""."

    Write-Verbose "$vm_ReturnObjOutput to pipeline."

    Write-Output $Word

} # End Convert-IntToWord function



