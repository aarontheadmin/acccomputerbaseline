﻿Function Format-ErrorMessage {

    Param ([string]$ComputerName,
           [string]$ErrorMessage)

    Write-Output "$($ComputerName.ToUpper()):`t$ErrorMessage`r`n"
}



